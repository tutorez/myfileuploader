/* global angular */
angular.module("main", ['ngFileUpload', 'ngImgCrop'])
	.controller("MainController", function ($scope, Upload) {
		$scope.upload = function (file) {
			console.log("the file is", file);

			//hey sheldon: there was a bug where ngf-select is fired before the file is uploaded, if that happens the file is null
			//and the server will return a 500. So I added this null check below so it doesnt send null files.			
			if (!file) return
			
			//also the default file type checking was not working for me but its easy to use:
			if (!file.type.startsWith("image/")) {
				console.log("not an image file... abort");
				return;
			}
			
			//this is so the image can be previewed, its not very clear in the docs
			$scope.file = file;

			Upload.upload({
				url: 'http://web.tutorez.com/api/FileApi/UploadFile',
				file: file,
				fields: { tutorID: 240 } //additional fields like tutorID
			}).progress(function (evt) {
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
			}).success(function (data, status, headers, config) {
				console.log('file ' + config.file.name + ' uploaded. Response: ', data);
				console.log($scope.file);
			}).error(function (data, status, headers, config) {
				console.log('error status: ' + status);
			})
		};
	})

	.controller('CropController', function ($scope, Upload) {

        $scope.myImage = '';
        $scope.myCroppedImage = '';

        var handleFileSelect = function (evt) {

			var file = evt.currentTarget.files[0];
			console.log(evt, file);
			var reader = new FileReader();
			reader.onload = function (evt) {
				$scope.$apply(function ($scope) {
					$scope.myImage = evt.target.result;
				});
			};
			reader.readAsDataURL(file);
        };
        angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);
			
		$scope.onChange=function(dataUri){
			cropAndSave(dataUri)
		}
		
		$scope.save= function () {
			cropAndSave($scope.myCroppedImage);
		}
		
		function cropAndSave(dataUri){
			if (!dataUri) return;
			console.log("myCroppedImage", dataUri);
			var blob = dataURItoBlob(dataUri);
			console.log("myCroppedImage", blob);

			Upload.upload({
				url: 'http://alpha.tutorez.com/api/FileApi/UploadFile',
				file: blob,
				fields: { tutorID: 240 } //additional fields like tutorID
			}).progress(function (evt) {
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				console.log('progress: ' + progressPercentage + '% ');
			}).success(function (data, status, headers, config) {
				console.log('file ' + ' uploaded. Response: ', data);
			}).error(function (data, status, headers, config) {
				console.log('error status: ' + status);
			})
		}
		
	});

function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], { type: mimeString });
}